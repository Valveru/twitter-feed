<?php

namespace App;

use Models\View; 

require "./../vendor/autoload.php";
require "./../config/config.php";

spl_autoload_extensions(".php");
spl_autoload_register(function ($class) {
    include './../' . $class . '.php';
});

$routes = include "./../config/routes.php";

if (empty($routes)) {
    throw new \Exception('Routes not found');
}
$url_data = parse_url('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);

if (
    $url_data['path'] &&
    array_key_exists($url_data['path'], $routes) &&
    isset($routes[$url_data['path']]['method']) &&
    $routes[$url_data['path']]['method'] === $_SERVER['REQUEST_METHOD']
) {

    try {
        $controller_class = '\\Controllers\\' . $routes[$url_data['path']]['controller'] . 'Controller';
        $controller = new $controller_class;
        //TODO: can make a parameter transfer
        $controller->{$routes[$url_data['path']]['action']}();
    } catch (\Exception $e) {
        $view = new View('errors', ['error' => $e->getMessage()]);
        $view->render();
    }
    return true;
}

$view = new View('errors');
$view->render();

