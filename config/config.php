<?php

ini_set('display_errors', true);
date_default_timezone_set('Europe/Kiev');

define('FEED_COUNT', 20);

define('CONSUMER_KEY', '');
define('CONSUMER_SECRET', '');
define('ACCESS_TOKEN',  '');
define('ACCESS_TOKEN_SECRET',  '');

define('CURRENT_TRANSPORT', 'Twitter');
define('UPDATE_FREQUENCY', 10000);

