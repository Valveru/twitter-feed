<?php

return [
    '/' => [
        'controller' => 'Index', 
        'action'    => 'index', 
        'method' => 'GET', 
    ],

    '/twits/api' => [
        'controller' => 'Api',
        'action'    => 'twits',
        'method' => 'POST', 
    ],
];

