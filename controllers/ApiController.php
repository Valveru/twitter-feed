<?php

namespace Controllers;


use Models\Controller;
use Models\View;
use Services\TransportService;

class ApiController extends Controller
{

    public function twits()
    {
        $service = TransportService::getInstance();
        $twits = $service->queryFeeds();
        
        $res = []; 
        
        if (!empty($twits)) {
            foreach ($twits as $twit) {
                $res[] = [
                    'user_name' => $twit->getUserName(),
                    'url' => $twit->getServiceUrl(),
                    'profile_image_url' => $twit->getProfileImg(),
                    'time' => date('H:i d-m-Y', $twit->getTime()),
                    'text' => $twit->getText(),
                ];
            }            
        }

        $view = new View('index', ['twits' => $res]);
        $view->renderJson();
    }
}
