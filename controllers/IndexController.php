<?php

namespace Controllers;


use Models\Controller;
use Models\View;
use Services\TransportService;

class IndexController extends Controller
{

    public function index()
    {
        $service = TransportService::getInstance();
        $twits = $service->queryFeeds();
        $view = new View('index', ['twits' => $twits]);
        $view->render();
    }
    
    
}
