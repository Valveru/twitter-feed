<?php

namespace Models;

class View
{
    private $data;
    private $name;
    private $view_dir_path;


    public function __construct($view_name, array $params = [])
    {
        $this->data = $params;
        $this->name = $view_name;
        $this->view_dir_path = __DIR__ . '/../views/';
    }

    public function renderJson()
    {
        echo json_encode(array_merge([
            'success' => true
        ], $this->data));
        exit;
    }

    public function render($layout = true)
    {
        $view_file_name = $this->view_dir_path . $this->name . '.php';
        if (!file_exists($view_file_name )) {
            throw new \Exception('Internal error: view is not exists');
        }

        extract($this->data);
        if ($layout) {
            include $this->view_dir_path . 'layout/layout.php';
        } else {
            include $view_file_name;
        }
        exit;
    }
}
