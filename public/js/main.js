function renderTwit(data) {
    var res = $('<div />', {class: 'tweet-list-item'}).append(
        $('<div />', {class:'avatar'}).append(
            $('<a />', {
                href: data.url,
                target: '_blank'
            }).append($('<img />', {
                src: data.profile_image_url,
                alt: data.user_name
            }))
        ),
        $('<div />', {class:'user'}).append(
            $('<a />', {
                href: data.url,
                target: '_blank',
                html: data.user_name
            })
        ),
        $('<div />', {class:'time', html: data.time}),
        $('<div />', {class:'txt', html: data.text})
    );
    return res;
}

$(function () {
    var update_frequency = $('meta[name=update_frequency]').attr('content');
    // setInterval(function () {
    //     console.log('Connecting...');
    //
    //     $.post('/twits/api', {
    //     }, function (resp) {
    //         try{
    //         } catch (e) {
    //             return false;
    //         }
    //         if (!resp || !resp.success) {
    //             console.error(resp);
    //         }
    //         if (resp.twits) {
    //             $('div.tweet-list-item').empty();
    //             for (var i in resp.twits) {
    //                 $('div.tweet-list').append(
    //                     renderTwit(resp.twits[i])
    //                 );
    //             }
    //         }
    //     });
    // }, update_frequency);
});