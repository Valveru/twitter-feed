<?php

namespace Services;

use Transports\iTransport;

class TransportService
{
    private static $dir =  '\\Transports\\';

    public static function getInstance(array $settings = [])
    {
        $class_name =  self::$dir . CURRENT_TRANSPORT . 'Transport';

        if (!class_exists($class_name)) {
            throw new \Exception("Unable to load class: $class_name");
        }

        $object = new $class_name($settings);
        if (!($object instanceof iTransport)) {
            throw new \Exception("Class \"$class_name\" is not implements iTransport");
        }

        return $object;
    }
}
