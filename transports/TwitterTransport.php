<?php

namespace Transports;

use Abraham\TwitterOAuth\TwitterOAuth;
use Transports\TwitterTransportItem;

class TwitterTransport implements iTransport
{
    private $connection;
    private $settings;
    
    public function __construct(array $settings = [])
    {
        $this->setSettings($settings); 
    }

    public function setSettings(array $settings = []) 
    {
        $this->settings = $settings; 
    }

    public function getConnection()
    {
        if (empty($this->connection)) {
                $this->connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
            $credentials = $this->connection->get('account/verify_credentials');

            if (empty($credentials) || isset($credentials->errors)) {
                $err_string = 'Can not connect.';
                $err = reset($credentials->errors);
                if (!empty($err) && isset($err->message)) {
                    $err_string .= ' ' . $err->message;
                }
                throw new \Exception($err_string);
            }
        }
        return $this->connection; 
    }
    
    public function getUserInfo() 
    {
       return $this->getConnection()->get('account/verify_credentials');
    }
    
    public function queryFeeds()
    {
        $user_info = $this->getUserInfo();
        $feeds = $this->getConnection()->get('statuses/user_timeline', [
            'screen_name' => $user_info->screen_name,
            'count' => FEED_COUNT,
        ]);

        $res = [];
        if (!empty($feeds)) {
            foreach ($feeds as $twit) {
                $twitter_item = new TwitterTransportItem;
                $twitter_item->setUserName($twit->user ? $twit->user->name : '');
                $twitter_item->setServiceUrl($twit->user ? 'http://twitter.com/' . $twit->user->screen_name : '');
                $twitter_item->setProfileImg($twit->user ? $twit->user->profile_image_url : '');
                $twitter_item->setTime($twit->created_at);
                $twitter_item->setText($twit->text);
                $res[] = $twitter_item;
            }
        }
        
        return $res; 
    }
    
    
}
