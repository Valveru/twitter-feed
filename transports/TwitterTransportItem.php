<?php

namespace Transports;

class TwitterTransportItem implements iFeedItem
{
   
    protected $user_name;
    protected $service_url;
    protected $profile_img;
    protected $time;
    protected $created_at;
    protected $text;
    
    public function __construct()
    {
        
    }

    public function getUserName() 
    {
        return $this->user_name; 
    }
    public function getServiceUrl()
    {
        return $this->service_url;
    }
    public function getProfileImg()
    {
        return $this->profile_img;
    }
    public function getTime()
    {
        return $this->time;
    }
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    public function getText()
    {
        return $this->text;
    }

    
    
    public function setUserName($data)
    {
        $this->user_name = $data;
    }
    public function setServiceUrl($data)
    {
        $this->service_url = $data;
    }
    public function setProfileImg($data)
    {
        $this->profile_img = $data;
    }
    public function setTime($data)
    {
        $this->time = $data;
    }
    public function setCreatedAt($data)
    {
        $this->created_at = strtotime($data);
    }
    public function setText($data)
    {
        $this->text = $data;
    }
    
}
