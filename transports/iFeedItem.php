<?php

namespace Transports;

interface iFeedItem
{
    public function getUserName();
    public function getServiceUrl();
    public function getProfileImg();
    public function getTime();
    public function getCreatedAt();
    public function getText();

    public function setUserName($data);
    public function setServiceUrl($data);
    public function setProfileImg($data);
    public function setTime($data);
    public function setCreatedAt($data);
    public function setText($data);
}
