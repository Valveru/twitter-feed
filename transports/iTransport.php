<?php

namespace Transports;

interface iTransport
{
    public function queryFeeds();
    public function getUserInfo();
}
