<h3>Latest twits : </h3>
<div class="tweet-list">
    <?php if (!empty($twits)): ?>
        <?php foreach ($twits as $twit): ?>
            <div class="tweet-list-item">
                <div class="avatar">
                    <a href="<?=$twit->getServiceUrl()?>" target="_blank"><img src="<?=$twit->getProfileImg()?>" alt="<?=$twit->getUserName()?>" /></a>
                </div>
                <div class="user"><a href="<?=$twit->getServiceUrl()?>" target="_blank"><?=$twit->getUserName()?></a></div>
                <div class="time"><?= date('H:i d-m-Y', $twit->getTime())?></div>
                <div class="txt"><?= $twit->getText() ?></div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
