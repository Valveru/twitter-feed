<!doctype html>
<html  lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="update_frequency" content="<?=UPDATE_FREQUENCY?>">
        <title>Twitter feed</title>
        <link rel="stylesheet" href="css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    </head>
    <body>
        <?php include $view_file_name ?>
    </body>
    <script src="js/main.js"></script>
</html>